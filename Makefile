
.PHONY: clean build update

clean:
	docker run --rm -it \
		-v $${HOME}/.m2:/root/.m2 \
		-v $(CURDIR):/coesra-keycloak-extensions \
		-w /coesra-keycloak-extensions \
		maven:3.8-openjdk-11 \
		mvn clean

# build the jar file
build:
	docker run --rm -it \
		-v $${HOME}/.m2:/root/.m2 \
		-v $(CURDIR):/coesra-keycloak-extensions \
		-w /coesra-keycloak-extensions \
		maven:3.8-openjdk-11 \
		mvn package

# used to update the extension when testing locally with keycloak running in docker-compose environment
update: build
	cp target/tern-keycloak-extension-3.0.0.jar ../volumes/deployments/tern-keycloak-extension.jar
