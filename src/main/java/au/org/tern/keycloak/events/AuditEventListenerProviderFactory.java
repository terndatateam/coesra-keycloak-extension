package au.org.tern.keycloak.events;

import org.jboss.logging.Logger;

import org.keycloak.Config;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventListenerProviderFactory;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;


/**
 * @author <a href="mailto:sthorger@redhat.com">Stian Thorgersen</a>
 */
public class AuditEventListenerProviderFactory implements EventListenerProviderFactory {

    public static final String ID = "tern-audit";

    // TODO: probably should wrap / sub class this somehow ...
    //       e.g. au.org.tern.audit.AuditLogger ...
    private static final Logger logger = Logger.getLogger("au.org.tern.audit");

    @Override
    public EventListenerProvider create(KeycloakSession session) {
        return new AuditEventListenerProvider(session, logger);
    }

    @Override
    public void init(Config.Scope config) {
        //  config.getArray("exclude-events");
        // init local variables to be passed on in create method
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
    }

    @Override
    public void close() {
    }

    @Override
    public String getId() {
        return ID;
    }

}
