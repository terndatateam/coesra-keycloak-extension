package au.org.tern.keycloak.events;

import org.jboss.logging.Logger;
import org.keycloak.events.Event;
import org.keycloak.events.EventListenerProvider;
import org.keycloak.events.EventType;
import org.keycloak.events.admin.AdminEvent;
// import org.keycloak.events.admin.OperationType;
import org.keycloak.models.KeycloakContext;
import org.keycloak.models.KeycloakSession;


import org.keycloak.util.JsonSerialization;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

import java.util.Map;
import java.util.HashMap;
import java.util.Set;

import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

/**
 * event overview:
 *
 * Login events:
 *      Login -
 *      Register -
 *      Logout -
 *      Code To Token -
 *      Refresh Token -
 *
 * Account events:
 *      social link -
 *      remove social link
 *      update email -
 *      update profile
 *      send password reset
 *      update password
 *      update totp
 *      remove totp
 *      send verify email
 *      verify email
 *
 *
 * <spi name="eventsListener">
 *   <provider name="<email>" enabled="true">
 *     <properties>
 *       <!-- json encoded example -->
 *       <property name="excluded-events" value="[&quot;UPDATE_TOTP&quot;,....]"/>
 *     </properties>
 *   </prodiver>
 * </spi>
 */

/**
 * @author <a href="mailto:sthorger@redhat.com">Stian Thorgersen</a>
 */
public class AuditEventListenerProvider implements EventListenerProvider {

    private final KeycloakSession session;
    private final Logger logger;

    private Set<EventType> includedEvents = Set.of(
        EventType.LOGIN, EventType.LOGIN_ERROR,
        EventType.REGISTER, EventType.REGISTER_ERROR,
        EventType.LOGOUT, EventType.LOGOUT_ERROR,
        EventType.CLIENT_LOGIN, EventType.CLIENT_LOGIN_ERROR,
        EventType.FEDERATED_IDENTITY_LINK, EventType.FEDERATED_IDENTITY_LINK_ERROR,
        EventType.REMOVE_FEDERATED_IDENTITY, EventType.REMOVE_FEDERATED_IDENTITY_ERROR,
        EventType.UPDATE_EMAIL, EventType.UPDATE_EMAIL_ERROR,
        EventType.UPDATE_PROFILE, EventType.UPDATE_PROFILE_ERROR,
        EventType.UPDATE_PASSWORD, EventType.UPDATE_PASSWORD_ERROR,
        EventType.UPDATE_TOTP, EventType.UPDATE_TOTP_ERROR,
        EventType.VERIFY_EMAIL, EventType.VERIFY_EMAIL_ERROR,
        EventType.VERIFY_PROFILE, EventType.VERIFY_PROFILE_ERROR,
        // EventType.IDENTITY_PROVIDER_LINK_ACCOUNT, EventType.IDENTITY_PROVIDER_LINK_ACCOUNT_ERROR,
        // EventType.IDENTITY_PROVIDER_LOGIN, EventType.IDENTITY_PROVIDER_LOGIN_ERROR,
        // EventType.IDENTITY_PROVIDER_FIRST_LOGIN, EventType.IDENTITY_PROVIDER_FIRST_LOGIN_ERROR,
        // EventType.IDENTITY_PROVIDER_POST_LOGIN, EventType.IDENTITY_PROVIDER_POST_LOGIN_ERROR,
        // EventType.IDENTITY_PROVIDER_RESPONSE, EventType.IDENTITY_PROVIDER_RESPONSE_ERROR,
        // EventType.IDENTITY_PROVIDER_RETRIEVE_TOKEN, EventType.IDENTITY_PROVIDER_RETRIEVE_TOKEN_ERROR,
        EventType.IMPERSONATE, EventType.IMPERSONATE_ERROR,
        EventType.DELETE_ACCOUNT, EventType.DELETE_ACCOUNT_ERROR
    );

    // public AuditEventListenerProvider(Set<EventType> excludedEvents, Set<OperationType> excludedAdminOpearations) {
    public AuditEventListenerProvider(KeycloakSession session, Logger logger) {
        this.session = session;
        this.logger = logger;
    }

    @Override
    public void onEvent(Event event) {
        if (includedEvents.contains(event.getType())) {
            logEvent(event);
        }
    }

    @Override
    public void onEvent(AdminEvent adminEvent, boolean includeRepresentation) {
        logAdminEvent(adminEvent, includeRepresentation);
    }

    // transaction callback to log collected events
    private void logEvent(Event event) {
        try {
            Map<String, Object> cadf = toMap(event);
            cadf.put("level", "AUDIT");
            logger.log(Logger.Level.INFO, JsonSerialization.writeValueAsString(cadf));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void logAdminEvent(AdminEvent event, boolean includeRepresentation) {
        try {
            Map<String, Object> cadf = toMap(event);
            cadf.put("level", "AUDIT");
            logger.log(Logger.Level.INFO, JsonSerialization.writeValueAsString(cadf));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Object noNull(Object obj) {
        if (obj == null) {
            return "";
        }
        return obj;
    }

    private Map<String, Object> toMap(Event event) {
        Map<String, Object> cadf = new HashMap<String, Object>();

        cadf.put("typeURI", "http://schemas.dmtf.org/cloud/audit/1.0/event");
        cadf.put("action", event.getType());  // authenticate
        if (event.getError() == null) {
            cadf.put("outcome", "success");
        } else {
            cadf.put("outcome", "failed");
            cadf.put("reason", event.getError());
        }
        ZonedDateTime zdt = ZonedDateTime.ofInstant(Instant.ofEpochMilli(event.getTime()), ZoneId.systemDefault());
        cadf.put("eventTime", zdt.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)); // iso 8601 + TZ
        // observer
        KeycloakContext context = session.getContext();
        UriInfo uriinfo = context.getUri();
        cadf.put("observer", Map.of(
            "id", "Keycloak", // app id / uri
            "addresses", Map.of(
                 // "name", uriinfo.getBaseUri(), // app name
                 "url", uriinfo.getBaseUri() // app url
            )
        ));
        // initiator
        cadf.put("initiator", Map.of(
            "id", noNull(event.getUserId()), // user id
            // "credentials", Map.of(
            //     "type", "", // auth method
            // ),
            "host", Map.of(
                "agent", noNull(context.getRequestHeaders().getHeaderString(HttpHeaders.USER_AGENT)), // user agent
                "ip", noNull(event.getIpAddress()) // source ip
            )
        ));

        Map<String, Object> target = new HashMap<String, Object>();
        target.put("id", "Keycloak"); // target id
        target.put("realmId", noNull(event.getRealmId()));
        target.put("clientId", noNull(event.getClientId()));
        if (event.getDetails() != null) {
            for (Map.Entry<String, String> e : event.getDetails().entrySet()) {
                target.put(e.getKey(), noNull(e.getValue()));
            }
        }

        cadf.put("target", target);
        //     // "addresses", [
        //     //     Map.of(
        //     //         "name", "", // app name
        //     //         "url", "", // request uri
        //     //     ),
        //     // ],
        // ));

        return cadf;
    }

    private Map<String, Object> toMap(AdminEvent event) {

        Map<String, Object> cadf = new HashMap<String, Object>();

        cadf.put("typeURI", "http://schemas.dmtf.org/cloud/audit/1.0/event");
        cadf.put("action", event.getOperationType());  // authenticate
        if (event.getError() == null) {
            cadf.put("outcome", "success");
        } else {
            cadf.put("outcome", "failed");
            cadf.put("reason", event.getError());
        }
        ZonedDateTime zdt = ZonedDateTime.ofInstant(Instant.ofEpochMilli(event.getTime()), ZoneId.systemDefault());
        cadf.put("eventTime", zdt.format(DateTimeFormatter.ISO_OFFSET_DATE_TIME)); // iso 8601 + TZ
        // observer
        KeycloakContext context = session.getContext();
        UriInfo uriinfo = context.getUri();
        cadf.put("observer", Map.of(
            "id", "Keycloak", // app id / uri
            "addresses", Map.of(
                 // "name", uriinfo.getBaseUri(), // app name
                 "url", uriinfo.getBaseUri(), // app url
                 "url2", context.getAuthServerUrl().toString()
            )
        ));
        cadf.put("initiator", Map.of(
            "id", noNull(event.getAuthDetails().getUserId()), // user id
            // "credentials", Map.of(
            //     "type", "", // auth method
            // ),
            "host", Map.of(
                "agent", noNull(context.getRequestHeaders().getHeaderString(HttpHeaders.USER_AGENT)), // user agent
                "ip", noNull(event.getAuthDetails().getIpAddress()) // source ip
            )
        ));
        cadf.put("target", Map.of(
            "id", "Keycloak", // target id
            // "<extras>", "<extra props",
            "realmId", noNull(event.getAuthDetails().getRealmId()),
            "clientId", noNull(event.getAuthDetails().getClientId()),
            "addresses", new Object[] {
                Map.of(
            //         "name", "", // app name
                    "type", noNull(event.getResourceTypeAsString()),
                    "url", noNull(event.getResourcePath()) // request uri
                )
            }
        ));
        return cadf;
    }

    @Override
    public void close() {
    }

}