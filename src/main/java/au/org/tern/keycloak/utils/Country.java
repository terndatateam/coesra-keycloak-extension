package au.org.tern.keycloak.utils;

/**
 * Mock Country class
 */
public class Country {
    String code;
    String name;

    public Country(String code, String name) {
        this.code = code;
        this.name = name;
    }
    
    public String getCode() {
        return this.code;
    }
    
    public void setCode(String code) {
        this.code = code;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "{" +
                " code='" + getCode() + "'" +
                ", name='" + getName() + "'" +
                "}";
    }
    
    public String countryCodeName() {
        return "(" + getCode() + ") " + getName();
    }

}
