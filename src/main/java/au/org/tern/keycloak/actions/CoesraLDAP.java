package au.org.tern.keycloak.actions;

import java.io.IOException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.naming.NamingException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.UriInfo;

import org.jboss.logging.Logger;
import org.keycloak.Config.Scope;
import org.keycloak.authentication.InitiatedActionSupport;
import org.keycloak.authentication.RequiredActionContext;
import org.keycloak.authentication.RequiredActionFactory;
import org.keycloak.authentication.RequiredActionProvider;
import org.keycloak.models.ClientModel;
import org.keycloak.models.GroupModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.models.UserModel;
import org.keycloak.models.utils.KeycloakModelUtils;
import org.keycloak.util.JsonSerialization;


// TODO: rename this class (and file name, and meta-inf services entry
public class CoesraLDAP implements RequiredActionProvider, RequiredActionFactory {
	private static Logger logger = Logger.getLogger(CoesraLDAP.class);
	private static Logger auditLogger = Logger.getLogger("au.org.tern.audit");

	public static final String PROVIDER_ID = "coesra-ldap-action";
	public static final String USER_ATTRIBUTE = "LDAP_ID";
	public static final String USER_DN_ATTRIBUTE = "LDAP_ENTRY_DN";

	// the client id for which this action provider will be active
	private String coesra_client_id = null;
	// ldap options
	private String ldap_basedn = null; // base dn to search for users
	private Integer ldap_default_gid = null; // default group id for new users
	private Integer ldap_start_uid = null; // minimal uid for new users
	private String home_root = null; // home base folder
	// User Group option
	private String coesra_user_group_path = null; // add users to this group if they sign in via the cofigured client

	private CoesraLDAPHelper ldap = null;

	// Allow clients to initiate this action via kc_action=PROVIDER_ID parameter in
	// authorize request
	@Override
    public InitiatedActionSupport initiatedActionSupport() {
        return InitiatedActionSupport.SUPPORTED;
    }

	@Override
	public RequiredActionProvider create(KeycloakSession session) {
		return this;
	}

	@Override
	public void init(Scope config) {
		ldap_basedn = config.get("basedn");
		ldap_default_gid = config.getInt("default-gid");
		ldap_start_uid = config.getInt("start-uid");
		coesra_client_id = config.get("client-id");
		coesra_user_group_path = config.get("coesra-user-group-path");
		home_root = config.get("home-root");
		try {
			ldap = new CoesraLDAPHelper(
				config.get("ldap-url"),
				config.get("ldap-authentication"),
				config.get("ldap-binddn"),
				config.get("ldap-bindpw")
			);
		} catch (NamingException e) {
			logger.error("Could not create LDAP Connection " + e.getMessage());
		}
	}


	@Override
	public void postInit(KeycloakSessionFactory factory) {
		// NOOP
	}

	@Override
	public void close() {
		// NOOP
	}

	@Override
	public String getId() {
		return PROVIDER_ID;
	}

	@Override
	public String getDisplayText() {
		return "Coesra LDAP Action";
	}

	/**
     * Called every time a user authenticates.  This checks to see if this required action should be triggered.
     * The implementation of this method is responsible for setting the required action on the UserModel.
     *
     * For example, the UpdatePassword required actions checks the password policies to see if the password has expired.
     *
     * @param context
     */
	@Override
	public void evaluateTriggers(RequiredActionContext context) {
		// Add user attribute LDAP_ID if does not exists.
		UserModel user = context.getUser();
		ClientModel client = context.getAuthenticationSession().getClient();
		String clientId = client.getClientId();

		// add action to set user attribute LDAP_ID if it is not set
		Object[] attr_list = user.getAttributeStream(USER_ATTRIBUTE).toArray();
		if (attr_list.length <= 0) {
			logger.info("Add user attribute LDAP_ID for user id " + user.getId());
			user.addRequiredAction(PROVIDER_ID);
		}
	
		if (! clientId.equals(coesra_client_id)) {
			// not coesra client ... ignore
			logger.debug("ignore client: " + client.getClientId() + " != " + coesra_client_id);
			return;
		}

		// check if we need to do anything for coesra client
		if (ldap == null) {
			logger.error("LDAP connection not configured");
			return;
		}

		// check if action is required for coesra client
		// check if user has ldap DN attributes set
		Object[] list = user.getAttributeStream(USER_DN_ATTRIBUTE).toArray();
		if (list.length <= 0) {
			// TODO: should probably check whether there is a value set as well
			// TODO: We also need some additional validation ... e.g. only do this for enabled / moderated accounts
			// attribute is not set, require this action so that ldap account get's created
			logger.info("Require LDAP account creation for user id " + user.getId());
			user.addRequiredAction(PROVIDER_ID);
		} else {
			logger.debug("user already in ldap: " + user.getId());
		}
		// check if user is member of group
		GroupModel coesraGroup = KeycloakModelUtils.findGroupByPath(context.getRealm(), coesra_user_group_path);
		if (coesraGroup != null && !user.isMemberOf(coesraGroup)) {
			user.addRequiredAction(PROVIDER_ID);
		}
		// all good ... nothing to do
	}

	/**
     * If the user has a required action set, this method will be the initial call to obtain what to display to the
     * user's browser.  Return null if no action should be done.
     *
     * @param context
     * @return
     */
	@Override
	public void requiredActionChallenge(RequiredActionContext context) {
		UserModel user = context.getUser();
		String username = null;
		String email = user.getEmail();
		String firstname = user.getFirstName();
		String lastname = user.getLastName();
		ClientModel client = context.getAuthenticationSession().getClient();
		try {
			// Set the user attribute LDAP_ID if not aleady set
			Object[] attr_list = user.getAttributeStream(USER_ATTRIBUTE).toArray();
			if (attr_list.length <= 0) {
				username = getUsername(user);
				user.setAttribute(USER_ATTRIBUTE, Arrays.asList(username));
			}
			else {
				// use existing LDAP_ID
				username = attr_list[0].toString();
			}

			if (! client.getClientId().equals(coesra_client_id)) {
				context.success();
				return;
			}
			
			// Below is for coesra client to set user attributes and add/update user in coesra LDAP
			// 1. check if username is in ldap
			// TODO: if ldap does not respond for some reason, then any ldap actions will take forever
			//       maybe skip all ldap actions in case attributes are set ? (otherwise ladp down is critical anyway)
			//       -> alert on LDAP errors here -> this affects everyone ....
			//          maybe not a big issue after all ? ... should only run on coesra client (verify), which is broken anyway if ldap is down
			if (! ldap.userExistInLDap(email, ldap_basedn)) {
				// no , let's create user in ldap
				logger.info("LDAP create account: " + username);
				// FIXME: need audit log here
				ldap.addUserToLdap(username, email, firstname, lastname, home_root,
						ldap_basedn, ldap_default_gid, ldap_start_uid);
				log_ldap_account_created(context, "success", username, null);
			}
			// TODO: might be nice to check if attributes actually change and log as audit event
			//       e.g. particularly interesting if attributes where null and we established a link with an existing ldap account
			// We are still here, so there was no exception or other error, let's update user and return success
			String userdn = "cn=" + username + "," + ldap_basedn;
			user.setAttribute(USER_DN_ATTRIBUTE, Arrays.asList(userdn));
			// TODO: createTimestamp, modifyTimestamp would be nice to get from ldap?
			GroupModel coesraGroup = KeycloakModelUtils.findGroupByPath(context.getRealm(), coesra_user_group_path);
			if (coesraGroup != null && !user.isMemberOf(coesraGroup)) {
				user.joinGroup(coesraGroup);
			}
		} catch (NamingException e) {
			// LDAP failed for whatever reason ...
			// TODO: we should notify user somehow? e.g. by presenting a challenge form to retry and / or other information to get support
			logger.error("Could not lookup / create user " + username + " in ldap: " + e.getMessage());
			log_ldap_account_created(context, "failed", username, e.toString());
		}
		// TODO: use failure, but also requires to return form here and implement processAction below
		context.success();
	}

	/**
     * Called when a required action has form input you want to process.
     *
     * @param context
     */
	@Override
	public void processAction(RequiredActionContext context) {
		// TODO: We can use this to retry ldap account creation.
		logger.warn("Process action not implemented yet.");
		context.success();
	}

	// FIXME/TODO: Ensure that username generated is unique across all users.
    protected String getUsername(UserModel user) {
        String email = user.getEmail();
        String coesraUserName = null;
        if(email != null && !email.trim().isEmpty()) {
        	String[] emailParts = email.split("@");
        	coesraUserName = emailParts[0] + "_" + emailParts[1].split("\\.")[0];
        } else {
        	// FIXME: should probably error here ?
			//        at least notify ?
        }
    	return coesraUserName;
    }

	private void log_ldap_account_created(RequiredActionContext context, String outcome, String username, String reason) {
		Map<String, Object> cadf = new HashMap<String, Object>();

		cadf.put("typeURI", "http://schemas.dmtf.org/cloud/audit/1.0/event");
        cadf.put("action", "coesra/ldap_account_create");  // authenticate
        cadf.put("outcome", outcome);
		if (outcome == "failed") {
			cadf.put("reason", reason);
		}
		cadf.put("eventTime", ZonedDateTime.now( ZoneOffset.UTC ).format( DateTimeFormatter.ISO_INSTANT ));
		// observer
		UriInfo uriinfo = context.getUriInfo();
		cadf.put("observer", Map.of(
			"id", "Keycloak",
			"addresses", Map.of(
				"url", uriinfo.getBaseUri()
			)
		));
		// initiator
		cadf.put("initiator", Map.of(
			"id", context.getUser().getId(),
			"host", Map.of(
				"agent", context.getHttpRequest().getHttpHeaders().getHeaderString(HttpHeaders.USER_AGENT),
				"ip", context.getConnection().getRemoteAddr()
			)
		));
		// target
		Map<String, Object> target = new HashMap<String, Object>();
		target.put("id", "Keycloak");
		target.put("realmId", context.getRealm().getId());
		target.put("clientId", context.getAuthenticationSession().getClient().getClientId());
		target.put("ldapId", username);
		cadf.put("target", target);

		try {
			auditLogger.log(Logger.Level.INFO, JsonSerialization.writeValueAsString(cadf));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
