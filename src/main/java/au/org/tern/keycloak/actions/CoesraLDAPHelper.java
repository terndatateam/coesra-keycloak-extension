package au.org.tern.keycloak.actions;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Clock;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Hashtable;
import java.util.UUID;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.BasicAttribute;
import javax.naming.directory.BasicAttributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;

// TODO: Move this to Kafka ????
public class CoesraLDAPHelper {

	private String ldapurl = null;
	private String authentication = "simple";
	private String binddn = null;
	private String bindpw = null;

	CoesraLDAPHelper(String url, String authentication, String binddn, String bindpw) throws NamingException {
		this.ldapurl = url;
		this.authentication = authentication;
		this.binddn = binddn;
		this.bindpw = bindpw;
	}

	private DirContext getContext() throws NamingException {
		Hashtable<String, String> env = new Hashtable<String, String>();
		env.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		// TODO: we may want to use connection pooling here?
		// env.put("com.sun.jndi.ldap.connect.pool", "true");
		// see also: https://docs.oracle.com/javase/jndi/tutorial/ldap/connect/config.html

		if (ldapurl != null) {
			env.put(Context.PROVIDER_URL, ldapurl);
		} else {
			throw new NamingException("LDAP URL is required");
		}

		// Authenticate if credentials were provided
		if (authentication != null && binddn != null && bindpw != null) {
			env.put(Context.SECURITY_AUTHENTICATION, authentication);
			env.put(Context.SECURITY_PRINCIPAL, binddn);
			env.put(Context.SECURITY_CREDENTIALS, bindpw);
		} else {
			throw new NamingException("Ldap Principle must not be null");
		}
		return new InitialDirContext(env);
	}

	public void addUserToLdap(String username, String email, String firstname, String lastname,
			String home_root, String basedn, Integer defaultGid, Integer startingUid) throws NamingException {
		Attributes attributes = new BasicAttributes();
		// object class
		Attribute objectClass = new BasicAttribute("objectClass");
		// top net needed !!!
		objectClass.add("top");
		objectClass.add("inetOrgPerson");
		objectClass.add("posixAccount");
		objectClass.add("shadowAccount");
		attributes.put(objectClass);
		username = username.toLowerCase();
		// sn
		attributes.put("sn", lastname);
		// cn
		attributes.put("cn", username);
		// mail
		attributes.put("mail", email);
		// organisation
		attributes.put("o", "CoESRA");
		// givenName
		attributes.put("givenName", firstname);
		// uid
		attributes.put("uid", username);
		// homedir
		// TODO: confiurable ?
		if(!home_root.endsWith("/")) {
			home_root += "/";
		}
		attributes.put("homeDirectory", home_root + username);
		// shadowLastChange
		LocalDate now = LocalDate.now(Clock.systemUTC());
		long days = ChronoUnit.DAYS.between(LocalDate.ofEpochDay(0), now);
		attributes.put("shadowLastChange", String.valueOf(days)); // 15140
		// shadowMin
		attributes.put("shadowMin", "0");
		// shadowMax
		attributes.put("shadowMax", "99999");
		// shadowWarning
		attributes.put("shadowWarning", "3");
		// gidNumber
		attributes.put("gidNumber", defaultGid.toString());
		// loginShell
		attributes.put("loginShell", "/bin/bash");
		// uidNumber
		attributes.put("uidNumber", getNextUid(basedn, startingUid).toString());
		// userPassword
		// password are generated randomly
		String _ramdomPassword = UUID.randomUUID().toString();
		attributes.put("userPassword", hashPassword(_ramdomPassword));
//		//businessCategory
//		attributes.put("businessCategory", user.getAreaOfResearch());
		String ldapPath = "cn=" + username + "," + basedn;
		getContext().createSubcontext(ldapPath, attributes);
	}

	/**
	 * whether user is in ldap
	 *
	 * @param user
	 * @return
	 * @throws NamingException
	 * @throws Exception
	 */
	public boolean userExistInLDap(String email, String ldapUserBaseDN) throws NamingException {
		String searchFilder = email;
		String searchFilter = String.format("mail=%s", escapeLDAPSearchFilter(searchFilder));
		NamingEnumeration<SearchResult> results = getContext().search(ldapUserBaseDN, searchFilter, new SearchControls());
		return results.hasMore();
	}

	/**
	 * Prevent LDAP injection
	 *
	 * @param filter LDAP filter string to escape
	 * @return escaped string
	 */
	private String escapeLDAPSearchFilter(String filter) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < filter.length(); i++) {
			char curChar = filter.charAt(i);
			switch (curChar) {
			case '\\':
				sb.append("\\5c");
				break;
			case '*':
				sb.append("\\2a");
				break;
			case '(':
				sb.append("\\28");
				break;
			case ')':
				sb.append("\\29");
				break;
			case '\u0000':
				sb.append("\\00");
				break;
			default:
				sb.append(curChar);
			}
		}
		return sb.toString();
	}

	private Integer getNextUid(String userBaseDN, int startingUid) throws NamingException {
		String searchFilter = "(cn=*)";
		SearchControls searchControls = new SearchControls();
		searchControls.setReturningAttributes(new String[] { "uidNumber" });
		NamingEnumeration<SearchResult> results = getContext().search(userBaseDN, searchFilter, searchControls);
		// is there any better way ?
		Integer highestUid = startingUid;
		while (results.hasMore()) {
			SearchResult result = results.next();
			Attribute uidNumberAtt = result.getAttributes().get("uidNumber");
			if (uidNumberAtt != null) {
				try {
					int uid = Integer.parseInt((String) uidNumberAtt.get());
					if (uid > highestUid)
						highestUid = uid;
				} catch (NumberFormatException e) {

				}
			}
		}
		return (highestUid + 1);
	}

	/**
	 * harsh a password
	 *
	 * @param s
	 * @return
	 */
	private String hashPassword(String s) {
		String generatedPassword = null;
		try {
			SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
			byte[] salt = new byte[16];
			sr.nextBytes(salt);

			MessageDigest md = MessageDigest.getInstance("SHA-256");
			md.update(salt);
			byte[] bytes = md.digest(s.getBytes());
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			// ignore
		}
		return generatedPassword;
	}
}
