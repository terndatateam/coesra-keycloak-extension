package au.org.tern.keycloak.actions;


import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.keycloak.Config.Scope;
import org.keycloak.authentication.DisplayTypeRequiredActionFactory;
import org.keycloak.authentication.InitiatedActionSupport;
import org.keycloak.authentication.RequiredActionContext;
import org.keycloak.authentication.RequiredActionFactory;
import org.keycloak.authentication.RequiredActionProvider;
import org.keycloak.forms.login.LoginFormsProvider;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.models.UserModel;
import org.keycloak.models.utils.FormMessage;
import org.keycloak.services.validation.Validation;

import au.org.tern.keycloak.utils.Country;
import au.org.tern.keycloak.utils.VocabHelper;

/**
 * A custom user profile validator action class
 * It is implemented as a RequiredActionProvider, RequiredActionFaction
 * and DisplayTypeRequiredActionFactory class.
 */
// TODO: can probably be done in custem user profile schema ?
public class UpdateProfileValidatorAction implements RequiredActionProvider, RequiredActionFactory, DisplayTypeRequiredActionFactory {

    private static final String PROVIDER_ID = "tern-update-profile-validation-action";
    private static final String ACTION_DISPLAY_NAME = "Tern Update and Validate Profile";
    // private static final String LOGIN_UPDATE_PROFILE_FORM = "login-update-profile.ftl";
    private static final UserModel.RequiredAction LOGIN_UPDATE_PROFILE_FORM = UserModel.RequiredAction.UPDATE_PROFILE;


    /**
     * Determines what type of support is provided for application-initiated
     * actions.
     *
     * @return InititatedActionsSupport
     */
    public InitiatedActionSupport initiatedActionSupport() {
        return InitiatedActionSupport.SUPPORTED;
    }

    /**
     * Methods of this class as a RequiredActionFactory
    */
    @Override
    public String getDisplayText() {
        return ACTION_DISPLAY_NAME;
    }

    @Override
    public RequiredActionProvider create(KeycloakSession session) {
        return this;
    }

    @Override
    public void init(Scope config) {
        // leave default implement

    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
        // leave default implement
    }

    @Override
    public String getId() {
        return PROVIDER_ID;
    }

    @Override
    public void close() {
    // leave default implement
    }


    /**
     * Methods of this class as a RequiredActionProvider
     */
    @Override
    public void evaluateTriggers(RequiredActionContext context) {
        // leave like - only implement it if
        // we want to trigger this action every time we find certain
        // user attributes missing
    }

    @Override
    public void requiredActionChallenge(RequiredActionContext context) {
        LoginFormsProvider formsProvider = context.form();

        // create form and issue a challenge i.e. show the form to user
        // Response challenge = formsProvider.createForm(LOGIN_UPDATE_PROFILE_FORM);
        // context.challenge(challenge);
        Response challenge = formsProvider.createResponse(LOGIN_UPDATE_PROFILE_FORM);
        context.challenge(challenge);

    }

    @Override
    public void processAction(RequiredActionContext context) {
        // Get data which has been sent by the user in the form
        MultivaluedMap<String, String> formData = context.getHttpRequest().getDecodedFormParameters();
        /**
         * Now get all the values in the update profile form
         * and challenge each value (i.e. validate each field)
         */

        // Validate Title field
        String formTitle = formData.getFirst("user.attributes.title");
        // TODO this is a mock call to external api for titles
        if (!Validation.isBlank(formTitle)) {
            String validatedTitle = VocabHelper.getTitles().stream()
                    .filter(value -> value.equals(formTitle))
                    .findFirst()
                    .orElse(null);

            // If title is null, issue another challenge
            // title must be one of the vocabs in list.
            if (validatedTitle == null) {
                createResponseWithError(context, "user.attributes.title", "invalidTitleMessage");
                return;
            }
            setFieldValueToModel(context, "title", validatedTitle);
        } else {
            // User did not opt for title, so save a blank - possibly
            // reset previous value in user's title attribute field
            setFieldValueToModel(context, "title", formTitle);
        }

        // Validate Orginisation field
        String formOrganisation = formData.getFirst("user.attributes.homeOrganisation");

        // If orginisation is null, issue another challenge
        //if (Validation.isBlank(formOrganisation) || formOrganisation.length() < 2)
        if (Validation.isBlank(formOrganisation)) {
            createResponseWithError(context, "user.attributes.homeOrganisation", "invalidHomeOrganisationMessage");
            return;
        }
        setFieldValueToModel(context, "homeOrganisation", formOrganisation);

        // Validate Country field
        String formCountryCode = formData.getFirst("user.attributes.country");
        // This a mock call to external api for orginisations
        Country validatedCountry = VocabHelper.getCountries().stream()
                .filter(value -> value.getCode().equals(formCountryCode))
                .findFirst()
                .orElse(null);

        // If country is null, issue another challenge
        if (validatedCountry == null) {
            createResponseWithError(context, "user.attributes.country", "invalidCountryMessage");
            return;
        }
        setFieldValueToModel(context, "countryName", validatedCountry.getName());
        setFieldValueToModel(context, "countryCode", validatedCountry.getCode());

        // Validate State if country is Australia
        if (validatedCountry.getName().equals("Australia")) {
            String formState = formData.getFirst("user.attributes.state");
            if (!Validation.isBlank(formState)) {
                String validateState = VocabHelper.getStates().stream()
                        .filter(value -> value.equals(formState))
                        .findFirst()
                        .orElse(null);

                // If state is null, issue another challenge
                // state must be one of the vocabs in list.
                if (validateState == null) {
                    createResponseWithError(context, "user.attributes.state", "invalidStateMessage");
                    return;
                }
                setFieldValueToModel(context, "state", validateState);
            } else {
                // Invalid situation. No place in Australia which does not belong to some State!
                // Issue a challenge.
                createResponseWithError(context, "user.attributes.state", "invalidStateMessage");
                return;
            }

        } else {
            // country not Australia
            // reset state - just in case has change country from Australia
            setFieldValueToModel(context, "state", "");
        }

        // Validate Field of Research (FoR) field
        String formFieldOfResearch = formData.getFirst("user.attributes.fieldOfResearch");
        // Note we used | pipe to delimiter items because some data has commas
         List<String> convertedFoRsList = Stream.of(formFieldOfResearch.split("\\|"))
                 .collect(Collectors.toList());

         // Limit selection to 3 items
         // Note: The ui also limits to 3 items selectable using selectize,
         // but we check here in the "model" to make sure its only upto 3 items
         if (convertedFoRsList.size() > 3) {
            createResponseWithError(context, "user.attributes.fieldOfResearch", "invalidItemsInFieldOfResearchMessage");
            return;
        }
        boolean forValid = true;
        // examine all FoRs supplied by the user to see if they validate
        for (String foRItem : convertedFoRsList) {
            String lookupFoR = VocabHelper.getFieldOfResearch().stream()
            .filter(value -> value.equals(foRItem))
            .findFirst()
                    .orElse(null);

            if (lookupFoR == null) {
                forValid = false;
           }
        }

        // If any invalid FoR, issue another challenge
        if (!forValid) {
            createResponseWithError(context, "user.attributes.fieldOfResearch", "invalidFieldOfResearchMessage");
            return;
        }
        setFieldValueToModel(context, "fieldOfResearch", formFieldOfResearch);

        // Validate Type of Activity (ToA) field
        String formTypeOfActivities = formData.getFirst("user.attributes.typeOfActivity");
        List<String> convertedToAsList = Stream.of(formTypeOfActivities.split("\\|"))
            .collect(Collectors.toList());
        // This is a mock call to external api for TOA vocabs
        boolean toaValid = true;
        // examine all TOAs supplied by the user
        for (String toa : convertedToAsList) {
            String lookupToA = VocabHelper.getTypeOfActivity().stream()
            .filter(value -> value.equals(toa))
            .findFirst()
                    .orElse(null);

            if (lookupToA == null) {
                toaValid = false;
           }
        }

        // If any ToA is invalid, issue another challenge
        if (!toaValid) {
            createResponseWithError(context, "user.attributes.typeOfActivity", "invalidTypeOfActivityMessage");
            return;
        }
        setFieldValueToModel(context, "typeOfActivity", formTypeOfActivities);

        // Great! everything validated. Continue
        context.success();

    }

    /**
     * Add validated attribute to model
     * Note: This also prevents loosing fields values
     * which have passed validation if form is re-rendered
     * after an invalid field.
     * @param context
     * @param attributeName
     * @param value
     */
    private void setFieldValueToModel(RequiredActionContext context, String attributeName, String value) {
        UserModel userModel = context.getUser();

        // Remove attribute if value is null, whitespaces " " or just empty ""
        if (value == null || value.trim().isEmpty()){
            // Remove attribute
            userModel.removeAttribute(attributeName);
        } else {
            // Save attribute value
            userModel.setSingleAttribute(attributeName, value);
        }
        return;
    }
    /**
     * Create a response with error.
     * @param context
     * @param formField
     * @param errorMessage
     */
    private void createResponseWithError(RequiredActionContext context, String formField, String errorMessage) {

        LoginFormsProvider formsProvider = context.form();

        Response challenge = formsProvider
                .addError(new FormMessage(formField, errorMessage))
                .createResponse(LOGIN_UPDATE_PROFILE_FORM);

        context.challenge(challenge);

        return;
    }

    // Implement this method from DisplayTypeRequiredActionFactory class
    @Override
    public RequiredActionProvider createDisplay(KeycloakSession session, String displayType) {
        // Return this class as a RequiredActionProvider object
        return this;
    }

}
