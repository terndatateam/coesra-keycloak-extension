# Keycloak Coesra LDAP integration

This extension creates LDAP user accounts when a user logs in via a specific client.
The functionality is implemented as required action in keycloak.

To enable this extension activate it as required action in the login flow. (`Coesra LDAP Action`)

This action checks whether an account already exists in LDAP. If not it will create the LDAP entry. The LDAP user name is generated from a users email address (`<email_user>_<first part of email domain>`).

If successful the action sets `LDAP_ID` (uid), and `LDAP_ENTRY_DN` (ldap user dn) as additional attributes on the user object in Keycloak.


## Configuration

Configuration is done via environmente variables

COESRA_LDAP_BASEDN: ldap base dn
COESRA_LDAP_DEFAULT_GID: default gid for new users
COESRA_LDAP_START_UID: minimum uid for users
COESRA_CLIENT_ID: the client for which this extension is active.

COESRA_LDAP_URL: ldap url
COESRA_LDAP_AUTHENTICATION: ldap auth method ("simple")
COESRA_LDAP_BINDDN: dn used to authenticate with ldap
COESRA_LDAP_BINDPW: ldap password for binddn


## Building

Make sure you have Java 11 and Maven installed.

```
mvn clean package
```

alternative us the plovided Makefile which uses docker images as build environment. (no need to install java or maven).

```
make build
```


## Deployment

Copy the release jar into the Keycloak `deployments` folder (`/opt/jboss/keycloak/standalone/deployments`)


## License

* [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0)
